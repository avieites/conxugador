[//]: # "This tool conjugates Galician verbs and can add them enclitic pronouns."
[//]: # "Copyright \(C\) 2020 Andrés Vieites Pérez"

[//]: # "This program is free software: you can redistribute it and/or modify"
[//]: # "it under the terms of the GNU General Public License as published by"
[//]: # "the Free Software Foundation, either version 3 of the License, or"
[//]: # "any later version."

[//]: # "This program is distributed in the hope that it will be useful,"
[//]: # "but WITHOUT ANY WARRANTY; without even the implied warranty of"
[//]: # "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the"
[//]: # "GNU General Public License for more details."

[//]: # "You should have received a copy of the GNU General Public License"
[//]: # "along with this program.  If not, see <https://www.gnu.org/licenses/>."

# Conxugador

Ferramenta para conxugar verbos en galego e con posibilidade de engadir pronomes de acusativo e dativo enclíticos.

## Modificacións dos paradigmas de conxugación de Conshuga

* Nos verbos que conxugan segundo o paradigma do verbo _facer_ aparece o verbo _benfazer_, foi modificado por _benfacer_
xa que en galego RAG/ILGA o verbo _facer_ e derivados empregan a grafía con _c_.
* No verbo _dicir_ a P5 do imperativo negativo (_IN_) aparece como <strike>digais</strike>, mudouse por _digades_.
* Substituíronse as entradas dos verbos _bendicir_ e _maldicir_ para que presenten a conxugación dupla
(ex: _bendigo_ - _bendizo_)

## Tests
* Para probarmos que o **Conxugador** dá os mesmos resultados que o _Conshuga_ creouse un ficheiro,
_all\_verbs\_in\_verbos.txt_ cun listado de todos os verbos con paradigma regular ou irregular que aparecen anotados no
documento _verbos.txt_ de _Conshuga_.
* Engadíronse ademais tres verbos regulares; abafar, ferver e rustrir; dos que non aparecen no documento _verbos.txt_
para probar que o **Conxugador** non só funciona correctamente para eses verbos.

## Construír e distribuír o paquete

Seguindo as instrucións en [python.org](https://packaging.python.org/tutorials/packaging-projects/) e tamén nesta [ligazón](https://python-packaging.readthedocs.io/en/latest/index.html) decidiuse seguir os pasos seguintes:

* Crear os ficheiros: README.md, setup.py, pyproject.toml e MANIFEST.in

  * En setup.py:

    * Para engadir o ficheiro `verbos.txt` (se non se especifica explicitamente ao non ser código python non o fai) empregamos a liña:

      ``````python
      package_data={"": ["verbos.txt"]},
      ``````

    * Para que o instalador habilite o paquete como un script / executábel sen termos a necesidade de explicitamente lanzalo con Python empregouse:

      ``````python
      entry_points={
              "console_scripts": [
                  "conxugador=conxugador.__main__:main"
              ]
          },
      ``````

* Executar o comando:

  ``````bash
  $ cd pyrepo/conxugador
  $ python -m build
  ``````
  
  Deste xeito xa temos no directorio `pyrepo\conxugador\dist` os ficheiros `conxugador-2021.4.tar.gz` e `conxugador-2021.4-py3-none-any.whl` que se poden instalar manualmente.
  
  * tar.gz: descomprimir e desde o cartafol descomprimido executar:
  
    ``````bash
    $ pip install .
    ``````
  
  * whl: executar:
  
    ``````bash
    $ pip install conxugador-2021.4-py3-none-any.whl
    ``````
  
* Instalar Twine (se non o está xa)

  ``````bash
  $ python -m pip install --user --upgrade twine
  ``````

* Finalmente súbese ao repo PyPI co comando:

  ``````bash
  $ python -m twine upload dist/*
  ``````

* E xa está subido en [PyPI](https://pypi.org/project/conxugador/).

