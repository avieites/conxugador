#!/usr/bin/perl -w

#
# Este ficheiro fai parte de Conshuga e está publicado
# baixo a licenza GNU General Public License 3
#
# Copyright (C) 2006-2011 ProLNat / Cilenis
#
# Vexa o ficheiro README para máis detalles
#

#use locale;
use POSIX;
#use locale(LC_CTYPE,"pt_PT");
use Getopt::Std;

our ($opt_h);
$Getopt::Std::STANDARD_HELP_VERSION = 1;
getopts('h');
sub HELP_MESSAGE {
    my $fh = shift;
    print $fh "Conshuga 1.0\n\n";
    print $fh "Utilización:\n\n";
    print $fh "1: ./conshuga.pl infinitivo\n";
    print $fh "  ou\n";
    print $fh "2: echo \"infinitivo\" | ./conshuga.pl\n\n";
    print $fh "./conshuga.pl -h mostra esta axuda\n";
}

sub VERSION_MESSAGE {
    my $fh = shift;
    print $fh "\nConshuga 1.0\n\n";
    print $fh "(C) 2006-2011 ProLNat / Cilenis\n\n";
}

if ($opt_h) {
    HELP_MESSAGE(*STDOUT);
    exit 1;
}

$file = "./verbos.txt" ;
open (FILE, "<$file") or die "O ficheiro 'verbos' non pode ser aberto: $!\n";

$verbo = shift(@ARGV);

if (!$verbo) {
    $verbo = <STDIN>;
    chomp($verbo);
    
    if (!$verbo || $verbo eq "") {
	HELP_MESSAGE(*STDOUT);
	exit 1;
    }
}

chomp($verbo);
$verbo = lc $verbo;
$verbo =~ tr/ÁÉÍÓÚÂÊÎÔÛÄËÏÖÜÑÇ/áéíóúâêîôûäëïöüñç/;

my $cons = "b|c|d|f|g|h|j|k|l|m|n|p|q|r|s|t|v|w|x|y|z|ç|ñ";
my $vog = "a|e|i|o|u";
my $vogal = "aeiou";

my $FN = "FN";
my $IP = "IP";

my $PI = "PI";
my $EI = "EI";
my $TI = "TI";
my $II = "II";
my $FI = "FI";
my $MI = "MI";

my $PS = "PS";
my $FS = "FS";
my $IS = "IS";

my $IA = "IA";
my $IN = "IN";


$nomeV{$FN} = "FORMAS NOMINAIS ";
$nomeV{$IP} = "INFINITIVO PERSOAL ";

$nomeV{$PI} = "PRESENTE DO INDICATIVO ";
$nomeV{$EI} = "PRETÉRITO PERFECTO DO INDICATIVO ";
$nomeV{$FI} = "FUTURO DO INDICATIVO ";
$nomeV{$TI} = "CONDICIONAL ";
$nomeV{$II} = "PRETÉRITO IMPERFECTO DO INDICATIVO ";
$nomeV{$MI} = "PRETÉRITO PLUSCUAMPERFECTO DO SUBXUNTIVO ";

$nomeV{$PS} = "PRESENTE DO SUBXUNTIVO ";
$nomeV{$FS} = "FUTURO DO SUBXUNTIVO " ;
$nomeV{$IS} = "PRETÉRITO IMPERFECTO DO SUBXUNTIVO ";

$nomeV{$IA} = "IMPERATIVO AFIRMATIVO ";
$nomeV{$IN} = "IMPERATIVO NEGATIVO ";

$verboESP = "";

if ($verbo =~ /(er|ar|ir|ír)$/) {
    ($verboR, $verboD) = ($verbo =~ /([a-zçñáéíóúüëïöü]*)(er|ar|ir|ír)$/);
    print STDERR "#$verboR# - #$verboD#\n";
}

else {
    $verboESP = $verbo;
    print STDERR "#$verboESP#\n";
}



while ($line = <FILE>) {
    chomp $line;

    if ($line =~ /^paradigma/) {
        $line = TirarSeparadores($line);
	($tmp, $paradigma) = split ('\:', $line) ;
    }

    if ($line =~ /^raiz/) {
	$line = TirarSeparadores($line);
        (@tmp) = split ('\:', $line) ;
        if ($#tmp == 1) {
	    $raiz1 = $tmp[1];
            $Raiz1{$paradigma} = $raiz1;
	} elsif ($#tmp == 2) {
	    $raiz1 = $tmp[1];
            $raiz2 = $tmp[2];
            $Raiz1{$paradigma} = $raiz1;
            $Raiz2{$paradigma} = $raiz2 ;
	} elsif ($#tmp == 3) {
	    $raiz1 = $tmp[1];
	    $raiz2 = $tmp[2];
	    $raiz3 = $tmp[3];
	    $Raiz1{$paradigma} = $raiz1;
	    $Raiz2{$paradigma} = $raiz2 ;
	    $Raiz3{$paradigma} = $raiz3 ;
	} elsif ($#tmp == 4) {
	    $raiz1 = $tmp[1];
	    $raiz2 = $tmp[2];
	    $raiz3 = $tmp[3];
	    $raiz4 = $tmp[4];
	    $Raiz1{$paradigma} = $raiz1;
	    $Raiz2{$paradigma} = $raiz2 ;
	    $Raiz3{$paradigma} = $raiz3 ;
	    $Raiz4{$paradigma} = $raiz4 ;
	}
    }

    if ($line =~ /^sufixo/) {
	$line =  TirarSeparadores($line);
        (@tmp) = split ('\:', $line) ;
        $sufixo = $tmp[1];
        $Sufixo{$paradigma} = $sufixo;
    }

    if ($line =~ /^FN/) {
	$Conj{$paradigma}{$FN} = $line;
    }
    if ($line =~ /^PI/) {
	$Conj{$paradigma}{$PI} = $line;
    }
    if ($line =~ /^II/) {
	$Conj{$paradigma}{$II} = $line;
    }
    if ($line =~ /^IA/) {
	$Conj{$paradigma}{$IA} = $line;
    }
    if ($line =~ /^IP/) {
	$Conj{$paradigma}{$IP} = $line;
    }
    if ($line =~ /^EI/) {
	$Conj{$paradigma}{$EI} = $line;
    }
    if ($line =~ /^MI/) {
	$Conj{$paradigma}{$MI} = $line;
    }
    if ($line =~ /^TI/) {
	$Conj{$paradigma}{$TI} = $line;
    }
    if ($line =~ /^FI/) {
	$Conj{$paradigma}{$FI} = $line;
    }
    if ($line =~ /^PS/) {
	$Conj{$paradigma}{$PS} = $line;
    }
    if ($line =~ /^IS/) {
	$Conj{$paradigma}{$IS} = $line;
    }
    if ($line =~ /^FS/) {
	$Conj{$paradigma}{$FS} = $line;
    }
    if ($line =~ /^IN/) {
	$Conj{$paradigma}{$IN} = $line;
    }

    if ($line =~ /^</) {
	$line = TirarSeparadores($line);
	(@tmp) = split ('\,', $line);
	for ($i=0; $i<=$#tmp; $i++) {
	    $Verbo{$paradigma}{$tmp[$i]}++;
	}
    }
}

$found=0;
foreach $p (keys %Verbo) {
    foreach $v (keys %{ $Verbo{$p} }) {
	if ($v eq $verbo) {
	
	    if ( (defined $Raiz1{$p}) &&
		 ($Raiz1{$p} eq "-") ) {
			ImprimirIrreg($p);
			$found=1;
	    } elsif ( (defined $Raiz1{$p}) &&
		      ($Raiz1{$p} eq "P") ) {
			$found=1;
			$Participio=1;
			ImprimirIrreg($p);
	    } elsif ( (defined $Raiz1{$p}) &&
		      ($Raiz1{$p} eq "D") ) {
			$found=1;
			$Duplo=1;
			ImprimirIrreg($p);
	    } elsif ( (defined $Raiz1{$p}) &&
		      ($Raiz1{$p} eq "DEF") ) {
			$found=1;
			$Defectivo=1;
			ImprimirIrreg($p);
	    } elsif (defined $Sufixo{$p}) {
			$sufixo = $Sufixo{$p};
			($prefixo) = ($p =~ /([a-zçñáéíóúüëïöü]*)$sufixo/i);
			($prefixoNovo) = ($verbo =~ /([a-zçñáéíóúüëïöü]*)$sufixo/i);
			
			print STDERR "Sufixo: $Sufixo{$p} -- Prefixo: $prefixo -- PrefixoNovo: $prefixoNovo\n";
			
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$prefixo/\:$prefixoNovo/g ;
				$Conj{$p}{$tempo} =~ s/\/$prefixo/\/$prefixoNovo/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "doer") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}# -- #$Raiz4{$p}#\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR4 = $verboR;
			$verboR2 =~ s/$/í/;
			$verboR3 =~ s/$/i/;
			$verboR4 =~ s/$/ï/;
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz4{$p}/\:$verboR4/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p =~ /enraizar$/) {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}# -- #$Raiz4{$p}# \n";
			print STDERR "Verbo regular con dúas raíces e listado\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR4 = $verboR;
			$verboR2 =~ s/i($cons)?z$/i$1c/;
			$verboR3 =~ s/i($cons)?z$/í$1z/;
			$verboR4 =~ s/i($cons)?z$/í$1c/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz4{$p}/\:$verboR4/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p =~ /cuincar$/) {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}# -- #$Raiz4{$p}# \n";
			print STDERR "Verbo regular con dúas raíces e listado\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR4 = $verboR;
			$verboR2 =~ s/i($cons)?c$/i$1qu/;
			$verboR3 =~ s/i($cons)?c$/í$1c/;
			$verboR4 =~ s/i($cons)?c$/í$1qu/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz4{$p}/\:$verboR4/g ;
			}
			ImprimirReg($p);
			$found=1;
	    } elsif ($p eq "muscar") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR2 =~ s/u($cons)?($cons)?($cons)?$/o$1$2$3/;
			$verboR3 =~ s/c$/qu/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "lucir") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR2 =~ s/u($cons)?($cons)?($cons)?$/o$1$2$3/;
			$verboR3 =~ s/c$/z/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ( ($p eq "afiar") || ($p eq "prohibir") ) {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/i($cons)?$/í$1/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ( ($p eq "saudar") || ($p eq "construír") || ($p eq "reunir") ) {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/u($cons)?$/ú$1/;
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "arruinar") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/($vog)i($cons)?$/$1í$2/;
			print STDERR "Raiz2: $verboR2\n";

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ( ($p eq "ferir") || ($p eq "despedir")) {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/e($cons)?($cons)?($cons)?$/i$1$2$3/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "saír") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/$/i/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "seguir") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/egu$/ig/;

			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "subir") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
			$verboR2 = $verboR;
			$verboR2 =~ s/u($cons)?($cons)?($cons)?$/o$1$2$3/;
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "xunguir") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}#\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR2 =~ s/u($cons)?($cons)?gu$/o$1$2gu/;
			$verboR3 =~ s/gu$/g/;
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "cubrir") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}#\n";
			$verboR2 = $verboR;
			$verboR3 = $verboR;
			$verboR2 =~ s/u($cons)?($cons)?($cons)?$/o$1$2$3/;
			$verboR3 =~ s/br$/b/;
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
				$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif ($p eq "crer") {
			print STDERR "Raíces escollidas: #$Raiz1{$p}# \n";
			
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			}
			ImprimirIrreg($p);
			$found=1;
	    } elsif (!defined $Raiz2{$p}) {
			print STDERR "Raíz escollida: #$Raiz1{$p}#\n";
			print STDERR "VERBO regular listado\n";
			foreach $tempo (keys %{$Conj{$p}}) {
				$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			}
			ImprimirReg($p);
			$found=1;
	    }
	}
    }
}

if ( (!$found) && (defined $verboR) ) {

    if ($verbo =~ /cer$/) {
        $p = "coñecer";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/c$/z/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /cir$/) {
        $p = "traducir";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/c$/z/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /guer$/) {
        $p = "abranguer";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/gu$/g/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /quer$/) {
        $p = "erquer";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/qu$/c/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /guir$/) {
        $p = "extinguir";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/gu$/g/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /guar$/) {
        $p = "minguar";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/gu$/gü/;
		print STDERR "OKKKK verbo2:: $verboR2\n";

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /güír$/) {
        $p = "argüír";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}#\n";
		$verboR2 = $verboR;
		$verboR3 = $verboR;
		$verboR2 =~ s/gü$/gu/;
        $verboR3 =~ s/gü$/gú/;

		foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /quir$/) {
        $p = "delinquir";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/qu$/c/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /uír$/) {
        $p = "construír";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/u$/ú/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /car$/) {
        $p = "comunicar";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/c$/qu/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /gar$/) {
        $p = "chegar";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/g$/gu/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /iuzar$/) {
		$p = "afiuzar";
		print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}# -- #$Raiz3{$p}# -- #$Raiz4{$p}# \n";
		print STDERR "Verbo regular con dúas raíces e listado\n";
		$verboR2 = $verboR;
		$verboR3 = $verboR;
		$verboR4 = $verboR;
		$verboR2 =~ s/uz$/uc/;
		$verboR3 =~ s/uz$/úz/;
		$verboR4 =~ s/uz$/úc/;
		
		foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz3{$p}/\:$verboR3/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz4{$p}/\:$verboR4/g ;
		}
		ImprimirReg($p);
		$found=1;
    } elsif ($verbo =~ /zar$/) {
        $p = "abrazar";
        print STDERR "Raíces escollidas: #$Raiz1{$p}# -- #$Raiz2{$p}#\n";
		$verboR2 = $verboR;
		$verboR2 =~ s/z$/c/;

        foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
			$Conj{$p}{$tempo} =~ s/\:$Raiz2{$p}/\:$verboR2/g ;
        }
		ImprimirReg($p);
		$found=1;
    } elsif ($verboD eq "ar") {
		$p = "cantar";
		print STDERR "Raíz escollida: #$Raiz1{$p}#\n";

		foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
		}
		ImprimirReg($p);
		$found=1;
    } elsif ($verboD eq "er") {
		$p = "vender";
		print STDERR "Raíz escollida: #$Raiz1{$p}#\n";
		
		foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
		}
		ImprimirReg($p);
		$found=1;
    } elsif ($verboD eq "ir") {
		$p = "partir";
		print STDERR "Raíz escollida: #$Raiz1{$p}#\n";

		foreach $tempo (keys %{$Conj{$p}}) {
			$Conj{$p}{$tempo} =~ s/\:$Raiz1{$p}/\:$verboR/g ;
		}
		ImprimirReg($p);
		$found=1;
    }
}

if (!$found) {
    print STDERR "Non foi recoñecido como verbo. Tente de novo\n\n"
}

sub TirarSeparadores {
    local ($x) = @_;

    $x =~ s/\<//g;
    $x =~ s/\>//g;
    $x =~ s/ //g;
    return $x
}

sub ImprimirReg {
    local ($x) = @_ ;

    print STDERR "## paradigma regular, deduzido de: <$x>\n\n";

    print "$Conj{$x}{$FN}\n";
    print "$Conj{$x}{$IP}\n";

    print "$Conj{$x}{$PI}\n";
    print "$Conj{$x}{$II}\n";
    print "$Conj{$x}{$EI}\n";
    print "$Conj{$x}{$MI}\n";
    print "$Conj{$x}{$TI}\n";
    print "$Conj{$x}{$FI}\n";

    print "$Conj{$x}{$PS}\n";
    print "$Conj{$x}{$IS}\n";
    print "$Conj{$x}{$FS}\n";

    print "$Conj{$x}{$IA}\n";
    print "$Conj{$x}{$IN}\n";

    return 1
}

sub ImprimirIrreg {
    local ($x) = @_ ;

    if (defined $Participio) {
	print STDERR "## paradigma participio irregular: <$x>\n\n";
    } elsif (defined $Duplo) {
	print STDERR "##Infinitivo que representa dous verbos : <$x-1|$x-2>\n\n";
    } elsif (defined $Defectivo) {
	print STDERR "##Verbo defectivo : <$x>\n\n";
    } else{
	print STDERR "## paradigma irregular: <$x>\n\n";
    }

    print "$Conj{$x}{$FN}\n";
    print "$Conj{$x}{$IP}\n";

    print "$Conj{$x}{$PI}\n";
    print "$Conj{$x}{$II}\n";
    print "$Conj{$x}{$EI}\n";
    print "$Conj{$x}{$MI}\n";
    print "$Conj{$x}{$TI}\n";
    print "$Conj{$x}{$FI}\n";

    print "$Conj{$x}{$PS}\n";
    print "$Conj{$x}{$IS}\n";
    print "$Conj{$x}{$FS}\n";

    print "$Conj{$x}{$IA}\n";
    print "$Conj{$x}{$IN}\n";

    return 1
}
