#!/bin/bash

# This tool conjugates Galician verbs and can add them enclitic pronouns.
# Copyright (C) 2020 Andrés Vieites Pérez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ORIGINAL_PATH=$(pwd)
SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
TOTAL_N_VERBS=757

i=0

while read line; do
	if [[ $line != \#* ]]; then
		### CONSHUGA
		# Running original Conshuga with all verbs in verbos.txt
		cd $SCRIPT_PATH/conshuga
		conshuga_output=$(mktemp)
	
		# Redirecting STERROR to null and appending STOUTPUT to $conshuga_output temp file
  		perl conshuga.pl $line 2> /dev/null 1>> $conshuga_output
  		
  		### CONXUGADOR
  		# Running Conxugador with all verbs in verbos.txt
		cd $SCRIPT_PATH/../../..
		conxugador_output=$(mktemp)
		
		# grep pipeline is required in order to remove the GNU GPL v3 notice in Conxugador output 
  		python3 conxugador $line | grep "^[A-Z][A-Z]:.*$" >> $conxugador_output
  		
  		if [ $? -ne 0 ]; then
  			printf "\nError during execution for '$line'\n"
  			break
  		fi 
  		
  		### COMPARISON
  		# -q, --brief
        #      report only when files differ
  		# -Z, --ignore-trailing-space
        #      ignore white space at line end
        # -Z is required because of a trailing space at the end of each line in Conshuga output
		if diff -qZ $conshuga_output $conxugador_output; then
			i=$((i+1))
			printf "%-20s[OK] (%2d %%)\n" $line $((i*100/TOTAL_N_VERBS))
		else
			printf "%-20s[ERROR] (diff -qZ %s %s)\n\n" $line $conshuga_output $conxugador_output
			printf "### CONSHUGA\n"
			cat $conshuga_output
			printf "\n### CONXUGADOR\n"
			cat $conxugador_output
			break
		fi
  	fi
done < $SCRIPT_PATH/all_verbs_in_verbos.txt

cd $ORIGINAL_PATH